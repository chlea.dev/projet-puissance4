################################################################################
# Votre travail consiste à compléter les fonctions ci-dessous afin de          #
#  contribuer à la détection d'une victoire                                    #
################################################################################
# Il s'agit bien sûr de respecter scrupuleusement les specifications.
# Un commentaire pour les lignes de code non triviales est exigé.
# Pour tester votre script, il suffit d'exécuter le programme JeuPuissance4.
# Le jeu se joue avec la souris uniquement, les joueurs se la passe à tour de role.
# Si vos fonctions sont justes alors le jeu fonctionnera !
# Bon courage.

def alignement_oblique_descendant(grille):  #droite
    ''' Cette fonction renvoie True si la grille contient un alignement oblique
        descendant de 4 pions de même couleur. Sinon elle renvoie False. '''
        
    for i in range (len(grille)-3):
        for j in range (len(grille[i])-3):
            if grille [i][j] != 0 :
                 if grille [i][j] == grille [i+1][j+1] == grille [i+2][j+2] == grille [i+3][j+3] :
                      return True
    return False    

            
            
def alignement_oblique_montant(grille):   #gauche
    ''' Cette fonction renvoie True si 4 pions de même couleur sont alignés
        horizontalement. Elle renvoie False sinon.   '''
    for i in range (len(grille)-3):
        for j in range (3, len(grille[i])):
            if grille [i][j] != 0 :
                if grille [i][j] == grille [i+1][j-1] == grille [i+2][j-2] == grille [i+3][j-3] :
                    return True
    return False

def alignement_oblique(grille):
    ''' Cette fonction renvoie True si la grille contient un alignement oblique
        de 4 pions de même couleur (que ce soit en descendant ou en montant.
        Sinon elle renvoie False. '''
    return  alignement_oblique_descendant(grille) or alignement_oblique_montant(grille)